package gt.umg.curso.pruebaandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class PreferenciasActivity extends AppCompatActivity {
private Button rbA1, rbA2, rbJ1, rbJ2, rbS1, rbS2;
    private String android, java, spring;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferencias);

        rbA1 = (Button) findViewById(R.id.rbA1);
        rbA2 = (Button) findViewById(R.id.rbA2);
        rbJ1= (Button) findViewById(R.id.rbJ1);
        rbJ2= (Button) findViewById(R.id.rbJ2);
        rbS1= (Button) findViewById(R.id.rbS1);
        rbS2= (Button) findViewById(R.id.rbS2);
    }
public void enviarPreferencias (View view){
    if (rbA1.isSelected()){
        android="SI";
    }else{
        android="No";
    }

    if (rbJ1.isSelected()){
        java="SI";
    }else{
        java="No";
    }

    if (rbS1.isSelected()){
        spring="SI";
    }else{
        spring="No";
    }

    Toast notificacion = Toast.makeText(this, "Android: "+ android+ "  "+
                                                "Java: "+java+ "  "+
                                                 "Spring: " + spring +" "+
                                            "Preferencias listas para monitoreo", Toast.LENGTH_LONG);
    notificacion.show();




}


}
