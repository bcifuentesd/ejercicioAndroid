package gt.umg.curso.pruebaandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText pass, confpass, nombre, apellido;
    private Button bt1,bt2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pass= (EditText)findViewById(R.id.etPassword);
        confpass=(EditText)findViewById(R.id.etCPassword);
        nombre=(EditText) findViewById(R.id.etNombre);
        apellido=(EditText) findViewById(R.id.etApellido);

    }
    public void enviar(View view) {
        Intent i = null;

        if (pass.getText().toString().isEmpty() || confpass.getText().toString().isEmpty() || nombre.getText().toString().isEmpty() || apellido.getText().toString().isEmpty()) {
            Toast notification = Toast.makeText(this, "Debe completar todos los campos", Toast.LENGTH_SHORT);
            notification.show();
        } else {


            if (pass.getText().toString().equals(confpass.getText().toString())) {
                i = new Intent(this, PreferenciasActivity.class);
                startActivity(i);
            } else {

                Toast notificacion = Toast.makeText(this, "Password no coincidecon Password de confirmación", Toast.LENGTH_LONG);
                notificacion.show();

            }
        }
    }
   public void cancelar (View view){
        pass.setText("");
        confpass.setText(" ");
        nombre.setText("");
        apellido.setText("");


    }

    }
